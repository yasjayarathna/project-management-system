<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function panelHome()
    {
        
        $totalProjects = Project::all()->count();
        $totalCompletedProjects = Project::where('status', 'Completed')->count();
        $totalOngoingProjects = Project::where('status', 'Ongoing')->count();
        $totalPendingProjects = Project::where('status', 'Pending')->count();
        
        return view('panel.dashboard.dashboard', ['totalProjects' => $totalProjects, 'totalCompletedProjects' => $totalCompletedProjects, 'totalOngoingProjects' => $totalOngoingProjects, 'totalPendingProjects' => $totalPendingProjects]);
    }
    
    
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function panelProjectManagerHome()
    {
        $totalProjects = Project::all()->count();
        $totalCompletedProjects = Project::where('status', 'Completed')->count();
        $totalOngoingProjects = Project::where('status', 'Ongoing')->count();
        $totalPendingProjects = Project::where('status', 'Pending')->count();
        
        return view('panel.dashboard.dashboard', ['totalProjects' => $totalProjects, 'totalCompletedProjects' => $totalCompletedProjects, 'totalOngoingProjects' => $totalOngoingProjects, 'totalPendingProjects' => $totalPendingProjects]);
    }
    
    
}
