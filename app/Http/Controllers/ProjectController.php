<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Task;

class ProjectController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_system_user');
//        $this->middleware('is_admin');
//        $this->middleware('is_project_manager');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $projects = Project::all();

//        foreach ($projects as $v) {
//
//            if ($v['is_admin']) {
//                $v['role'] = 'Admin';
//            } elseif ($v['is_project_manager']) {
//                $v['role'] = 'Project Manager';
//            } else {
//                $v['role'] = 'Normal User';
//            }
//
//        }

        return view('panel.project.projects', ['projects' => $projects]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('panel.project.project_add');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name'  => 'required',
            'startdate' => 'required',
            'enddate' => 'required',
            'status' => 'required',
        ]);
        
        if ($request->submit) {

            $project = new Project();
            $project->name = $request->name;
            $project->description = $request->description;
            $project->start_date = date("Y-m-d", strtotime($request->startdate));
            $project->end_date = date("Y-m-d", strtotime($request->enddate));
            $project->status = $request->status;

            try {
                $project->save();
                return redirect()->route('projects.edit', ['id' => $project->id])->with('success', 'Project added successfully!');
            } catch (\Exception $ex) {
                // do task when error
                echo $ex->getMessage();
            }
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $project = Project::findOrFail($id);
        
        $tasks = $project->tasks;
        
        return view('panel.project.project_view', ['project' => $project, 'tasks' => $tasks]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $project = Project::findOrFail($id);
        
        $tasks = $project->tasks;

        return view('panel.project.project_edit', ['project' => $project, 'tasks' => $tasks]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'name'  => 'required',
            'startdate' => 'required',
            'enddate' => 'required',
            'status' => 'required',
        ]);
        
        if ($request->submit) {

            $project = Project::find($id);
            $project->name = $request->name;
            $project->description = $request->description;
            $project->start_date = date("Y-m-d", strtotime($request->startdate));
            $project->end_date = date("Y-m-d", strtotime($request->enddate));
            $project->status = $request->status;

            try {
                $project->save();
                return redirect()->route('projects.edit', ['id' => $project->id])->with('success', 'Project updated successfully!');
            } catch (\Exception $ex) {
                // do task when error
                echo $ex->getMessage();
            }
            
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $project = Project::find($id);
        
        if ($project->delete()) {
            return response()->json(['status' => true, 'message' => 'Successfully deleted.']);
        } else {
            return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again.']);
        }
        
    }
}
