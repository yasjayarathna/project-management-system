<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class SystemUserController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $users = User::all();
        
        foreach ($users as $v) {
                
            if ($v['is_admin']) {
                $v['role'] = 'Admin';
            } elseif ($v['is_project_manager']) {
                $v['role'] = 'Project Manager';
            } else {
                $v['role'] = 'Normal User';
            }

        }

        return view('panel.user.users', ['users' => $users]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $roles = array (
            'is_admin' => 'Administrator',
            'is_project_manager' => 'Project Manager',
            'is_normal' => 'Normal User'
        );
        
        return view('panel.user.user_add', ['roles' => $roles]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name'  => 'required',
            'email' => 'required|email|unique:users,email',
            'role' => 'required',
            'password'   => 'required|required_with:password_confirmation|same:password_confirmation|min:8',
            'password_confirmation' => 'required',
        ]);

        if ($request->submit) {

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->role == 'is_admin') {
                $user->is_admin = true;
            } elseif ($request->role == 'is_project_manager') {
                $user->is_project_manager = true;
            }
            $user->password = Hash::make($request->password);
            $user->created_at = new \DateTime();
            $user->updated_at = new \DateTime();

            try {
                $user->save();
                return redirect('users')->with('success', 'User added successfully!');
            } catch (\Exception $ex) {
                // do task when error
                echo $ex->getMessage();
            }
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $user_info = User::findOrFail($id);
        
        $roles = array (
            'is_admin' => 'Administrator',
            'is_project_manager' => 'Project Manager',
            'is_normal' => 'Normal User'
        );
        
        $role = '';
        if ($user_info['is_admin'] == true) {
            $role = 'is_admin';
        } elseif ($user_info['is_project_manager'] == true) {
            $role = 'is_project_manager';
        } else {
            $role = 'is_normal';
        }
        
        
        return view('panel.user.user_edit', ['user_info' => $user_info, 'roles' => $roles, 'role' => $role]);
    
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'name'  => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'role' => 'required',
            'password'   => 'sometimes|same:password_confirmation'
        ]);
        
        if ($request->submit) {

            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->role == 'is_admin') {
                $user->is_admin = true;
                $user->is_project_manager = false;
            } elseif ($request->role == 'is_project_manager') {
                $user->is_admin = false;
                $user->is_project_manager = true;
            } else {
                $user->is_admin = false;
                $user->is_project_manager = false;
            }
            $user->password = Hash::make($request->password);
            $user->created_at = new \DateTime();
            $user->updated_at = new \DateTime();

            try {
                $user->save();
                return redirect('users')->with('success', 'User updated successfully!');
            } catch (\Exception $ex) {
                // do task when error
                echo $ex->getMessage();
            }
            
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        
        if ($user->delete()) {
            return response()->json(['status' => true, 'message' => 'Successfully deleted.']);
        } else {
            return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again.']);
        }
    }
}
