<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Task;

class TaskController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_system_user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        
        $project = Project::findOrFail($id);
        
        return view('panel.task.task_add', ['project' => $project]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name'  => 'required',
            'duration' => 'required',
            'status' => 'required',
        ]);
        
        if ($request->submit) {

            $task = new Task();
            $task->project_id = $request->project;
            $task->name = $request->name;
            $task->description = $request->description;
            $task->duration = $request->duration;
            $task->status = $request->status;
            $task->updated_at = new \DateTime();

            try {
                $task->save();
                return redirect()->route('projects.edit', ['id' => $request->project])->with('success', 'Task added successfully!');
            } catch (\Exception $ex) {
                // do task when error
                echo $ex->getMessage();
            }
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $task = Task::findOrFail($id);
        
        $project = $task->project;

        return view('panel.task.task_edit', ['task' => $task, 'project' => $project]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'name'  => 'required',
            'duration' => 'required',
            'status' => 'required',
        ]);
        
        if ($request->submit) {

            $task = Task::find($id);
            $task->project_id = $request->project;
            $task->name = $request->name;
            $task->description = $request->description;
            $task->duration = $request->duration;
            $task->status = $request->status;
            $task->updated_at = new \DateTime();

            try {
                $task->save();
                return redirect()->route('projects.edit', ['id' => $request->project])->with('success', 'Task updated successfully!');
            } catch (\Exception $ex) {
                // do task when error
                echo $ex->getMessage();
            }
            
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $task = Task::find($id);
        
        if ($task->delete()) {
            return response()->json(['status' => true, 'message' => 'Successfully deleted.']);
        } else {
            return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again.']);
        }
        
    }
}
