<?php

namespace App\Http\Middleware;

use Closure;

class IsSystemUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        return $next($request);
        if(auth()->user()->is_admin == 1 || auth()->user()->is_project_manager == 1) {
            return $next($request);
        }

        return redirect('home')->with('error', "You don't have section access.");
    }
}
