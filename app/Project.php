<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'start_date', 'end_date', 'status'
    ];
    
    
    /**
     * Get the tasks for the project.
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    
    // this is a recommended way to declare event handlers
    public static function boot() {
        
        parent::boot();
        self::deleting(function($project) { // before delete() method call this
             $project->tasks()->each(function($task) {
                $task->delete(); // <-- direct deletion
             });
             // do the rest of the cleanup...
        });
        
    }
    
    
}
