<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id', 'name', 'description', 'start_date', 'end_date', 'status'
    ];
    
    /**
     * Get the project that owns the task.
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
    
}
