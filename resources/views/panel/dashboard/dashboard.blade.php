@extends('panel.layouts.app')

@section('content')

<div class="container-fluid">
    
    <h2 class="mt-4">ABC Constructions (PVT) Ltd</h2>
    
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card bg-primary text-white mb-4">
                <div class="card-body">Total Projects</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <strong>{{$totalProjects}}</strong>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-warning text-white mb-4">
                <div class="card-body">Total Completed Projects</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <strong>{{$totalCompletedProjects}}</strong>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-success text-white mb-4">
                <div class="card-body">Total Ongoing Projects</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <strong>{{$totalOngoingProjects}}</strong>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-danger text-white mb-4">
                <div class="card-body">Total Pending Projects</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <strong>{{$totalPendingProjects}}</strong>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection