<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <?php
                    $user = Auth::user();
                ?>
                <a class="nav-link" href="{{$user->is_admin == 1 ? url('admin/home') : url('projectmanager/home') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-tachometer-alt"></i>
                    </div>
                    Dashboard
                </a>
                
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProjects" aria-expanded="false" aria-controls="collapseProjects">
                    <div class="sb-nav-link-icon"><i class="fas fa-object-group"></i></div>
                        Projects
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseProjects" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{url('/projects')}}">View All</a>
                        <a class="nav-link" href="{{url('projects/create')}}">Add New</a>
                    </nav>
                </div>
                
                @auth
                    <?php
                        $user = Auth::user();
                    ?>
                    @if ($user->is_admin == 1)
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSystemUsers" aria-expanded="false" aria-controls="collapseSystemUsers">
                            <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                Users
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseSystemUsers" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{url('/users')}}">View All</a>
                                <a class="nav-link" href="{{url('users/create')}}">Add New</a>
                            </nav>
                        </div>
                    @endif
                @endauth
                
            </div>
        </div>
        
    </nav>
</div>