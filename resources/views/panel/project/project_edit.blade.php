@extends('panel.layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Project</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-6 col-md-6">
            
            <div class="card mb-4">
                
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                </div>
                
                <div class="card-body">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('/projects/'.$project['id'].'/update') }}" method="POST">
                        @csrf
                        
                        <div class="form-group">
                            <label for="name">Name<span class="text-danger">*</span></label>
                            <input type="text" name="name" value="{{$project['name']}}" class="form-control" placeholder="Enter name" id="name">
                        </div>
                        
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control" rows="3" id="description">{{$project['description']}}</textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="startdate">Start Date<span class="text-danger">*</span></label>
                            <input type="text" class="datepicker form-control" value="{{date('Y-m-d', strtotime($project['start_date']))}}" name="startdate" placeholder="Enter start date" id="startdate">
                        </div>
                        
                        <div class="form-group">
                            <label for="enddate">End Date<span class="text-danger">*</span></label>
                            <input type="text" class="datepicker form-control" value="{{date('Y-m-d', strtotime($project['end_date']))}}"  name="enddate" placeholder="Enter start date" id="enddate">
                        </div>
                        
                        <div class="form-group">
                            <label for="enddate">Status<span class="text-danger">*</span></label>
                            <select class="form-control" name="status" id="status">
                                <option value="Pending" {{ $project['status'] == "Pending" ? "selected" : "" }}>Pending</option>
                                <option value="Ongoing" {{ $project['status'] == "Ongoing" ? "selected" : "" }}>Ongoing</option>
                                <option value="Ontesting" {{ $project['status'] == "Ontesting" ? "selected" : "" }}>Ontesting</option>
                                <option value="Completed" {{ $project['status'] == "Completed" ? "selected" : "" }}>Completed</option>
                            </select>
                        </div>
                        
                        <button name="submit" value="submit" type="submit" class="btn btn-primary">Update</button>
                        
                    </form>

                </div>
                
            </div>
            
        </div>
        
        <div class="col-xl-6 col-md-6">
            
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active justify-content-between" style="width: 100%;" aria-current="page">
                        Project Tasks
                        <a class="btn btn-primary" style="cursor: pointer;" href="{{ url('/tasks/create/'.$project['id']) }}">Add Task</a>
                    </li>
                </ol>
            </nav>
            
            <span>
                <div id="message-success" class="alert alert-success" role="alert" style="display: none;"></div>
                <div id="message-error" class="alert alert-danger" role="alert" style="display: none;"></div>
            </span>
            
            <div id="accordion">
                
                @foreach ($tasks as $r)
                
                    <div class="card" id="task-card-{{$r['id']}}">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link d-flex justify-content-between" style="width: 100%;" data-toggle="collapse" data-target="#collapse-{{$r['id']}}" aria-expanded="false" aria-controls="collapse-{{$r['id']}}">
                                    <span id="task-name-{{$r['id']}}">
                                        {{$r['name']}}
                                        @if ($r['status'] == "Pending")
                                            <span class="badge badge-primary">Pending</span>
                                        @elseif ($r['status'] == "Ongoing")
                                            <span class="badge badge-warning">Ongoing</span>
                                        @elseif ($r['status'] == "Ontesting")
                                            <span class="badge badge-info">Ontesting</span>
                                        @else
                                            <span class="badge badge-success">Completed</span>
                                        @endif
                                    </span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapse-{{$r['id']}}" class="collapse" aria-labelledby="heading-{{$r['id']}}" data-parent="#accordion">
                            <div class="card-body">
                                <input type="text" id="csrf_token_delete_task_{{$r['id']}}" name="csrf_token_delete_task_{{$r['id']}}" value="{{csrf_token()}}" hidden/>
                                
                                <span class="float-right task-delete" data-id="{{$r['id']}}" id="task-delete-{{$r['id']}}" style="cursor: pointer;">
                                    <i class="fas fa-trash-alt"></i>
                                </span>
                                
                                <a class="float-right mr-4" style="display: block;" href="{{ url('/tasks/'.$r['id']) }}">
                                    <i class="fas fa-edit"></i>
                                </a>
                                
                                <h6 class="card-subtitle mb-2 text-muted" id="task-duration-{{$r['id']}}">Duration :- {{$r['duration']}} Days</h6>
                                <p class="card-text" id="task-description-{{$r['id']}}">{{$r['description']}}</p>
                                  
                            </div>
                        </div>
                    </div>
                
                @endforeach
                
            </div>
            
        </div>
        
    </div>
    
</div>

@endsection

@section('javascript')

<script>
    
    $('.datepicker').datepicker({
        todayHighlight: true,
        autoclose: true
    });
    
    
    // delete task
    // -----------------------------------------.
    $(".task-delete").on('click', function () {

        var task_id = $(this).data('id');
        var csrf_token = $("#csrf_token_delete_task_"+task_id).val(); 
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrf_token
            }
        });
        
        $.ajax({
            url: SITE_URL+'/tasks/'+task_id+'/destroy',
            type: 'POST',
            dataType: 'json',
            data: {
                task_id : task_id
            }
        }).always(function (data) {
            
            if (data.status) {
                $('#task-card-'+task_id).remove();
                $('#message-success').text(data.message);
                $('#message-success').show('slow').delay(2000).hide('slow');
            } else {
                $('#message-error').text(data.message);
                $('#message-error').show('slow').delay(2000).hide('slow');
            }

        });

        return false;
        
    });
    
</script>

@endsection