@extends('panel.layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Project</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">
                
                <div class="card-body">
                    
                    <h5>{{$project['name']}}</h5>
                    
                    <p>
                        {{$project['description']}}
                    </p>
                    
                    <p>
                        <strong>Start Date :-</strong> {{date('Y-m-d', strtotime($project['start_date']))}}
                    </p>
                    
                    <p>
                        <strong>End Date :-</strong> {{date('Y-m-d', strtotime($project['end_date']))}}
                    </p>
                    
                    <p>
                        <strong>Status :- </strong>
                        @if ($project['status'] == "Pending")
                            <span class="badge badge-primary">Pending</span>
                        @elseif ($project['status'] == "Ongoing")
                            <span class="badge badge-warning">Ongoing</span>
                        @elseif ($project['status'] == "Ontesting")
                            <span class="badge badge-info">Ontesting</span>
                        @else
                            <span class="badge badge-success">Completed</span>
                        @endif
                    </p>

                </div>
                
                <div class="card">
                    <div class="card-header">
                        <strong>Project Tasks</strong>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            @foreach ($tasks as $r)
                                <li class="list-group-item">
                                    
                                    {{$r['name']}}
                                    
                                    @if ($r['status'] == "Pending")
                                        <span class="badge badge-primary">Pending</span>
                                    @elseif ($r['status'] == "Ongoing")
                                        <span class="badge badge-warning">Ongoing</span>
                                    @elseif ($r['status'] == "Ontesting")
                                        <span class="badge badge-info">Ontesting</span>
                                    @else
                                        <span class="badge badge-success">Completed</span>
                                    @endif
                                    
                                    <h6 class="card-subtitle mt-2 mb-2 text-muted">
                                        <strong>Duration :- </strong> {{$r['duration']}} Days
                                    </h6>
                                    
                                    <p>
                                        {{$r['description']}}
                                    </p>
                                    
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                
            </div>
            
        </div>
        
    </div>
    
</div>

@endsection

