@extends('panel.layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Projects</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">
                
                <span>
                    <div id="message-success" class="alert alert-success" role="alert" style="display: none;"></div>
                    <div id="message-error" class="alert alert-danger" role="alert" style="display: none;"></div>
                </span>
                
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                </div>
                
                <div class="card-body">

                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <!--<th scope="col">#</th>-->
                                <th scope="col">Name</th>
                                <th scope="col" style="width: 30%;">Description</th>
                                <th scope="col">Start Date</th>
                                <th scope="col">End Date</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($projects as $p) {
                            ?>
                            <tr id="project-{{$p['id']}}">
                                <!--<td><?php echo $p['id']; ?></td>-->
                                <td>{{$p['name']}}</td>
                                <td>{{$p['description']}}</td>
                                <td>{{date('Y-m-d', strtotime($p['start_date']))}}</td>
                                <td>{{date('Y-m-d', strtotime($p['end_date']))}}</td>
                                <td>
                                    <a class="btn btn-success" href="{{ url('/projects/'.$p['id'].'/view') }}">View</a>
                                    &nbsp;&nbsp;
                                    <a class="btn btn-primary" href="{{ url('/projects/'.$p['id']) }}">Edit</a>
                                    &nbsp;&nbsp;
                                    <button class="btn btn-danger project-delete" data-id="{{$p['id']}}">Delete</button>
                                    <input type="text" id="csrf_token_delete_project_{{$p['id']}}" name="csrf_token_delete_task_{{$p['id']}}" value="{{csrf_token()}}" hidden/>
                                </td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>

                </div>

            </div>
            
        </div>
    </div>
    
</div>

@endsection

@section('javascript')

<script>
    
    // delete project
    // -----------------------------------------.
    $(".project-delete").on('click', function () {

        var project_id = $(this).data('id');
        var csrf_token = $("#csrf_token_delete_project_"+project_id).val(); 
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrf_token
            }
        });
        
        $.ajax({
            url: SITE_URL+'/projects/'+project_id+'/destroy',
            type: 'POST',
            dataType: 'json',
            data: {
                project_id : project_id
            }
        }).always(function (data) {
            
            if (data.status) {
                $('#project-'+project_id).remove();
                $('#message-success').text(data.message);
                $('#message-success').show('slow').delay(2000).hide('slow');
            } else {
                $('#message-error').text(data.message);
                $('#message-error').show('slow').delay(2000).hide('slow');
            }

        });

        return false;
        
    });
    
</script>

@endsection