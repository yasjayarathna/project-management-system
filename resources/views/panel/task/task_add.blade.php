@extends('panel.layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Task</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('/tasks') }}" method="POST">
                        @csrf
                        
                        <div class="form-group">
                            <label for="project">Project<span class="text-danger">*</span></label>
                            <input type="text" name="project" value="{{$project['id']}}" class="form-control" id="project" hidden="hidden">
                            <input type="text" name="dis-project" value="{{$project['name']}}" class="form-control" id="project" disabled="disabled">
                        </div>
                        
                        <div class="form-group">
                            <label for="name">Name<span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" placeholder="Enter name" id="name">
                        </div>
                        
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control" rows="3" id="description"></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="duration">Duration(Days)<span class="text-danger">*</span></label>
                            <input type="number" class="form-control" name="duration" placeholder="Enter Duration" id="duration">
                        </div>
                        
                        <div class="form-group">
                            <label for="enddate">Status<span class="text-danger">*</span></label>
                            <select class="form-control" name="status" id="status">
                                <option value="Pending">Pending</option>
                                <option value="Ongoing">Ongoing</option>
                                <option value="Ontesting">Ontesting</option>
                                <option value="Completed">Completed</option>
                            </select>
                        </div>
                                                
                        <button name="submit" value="submit" type="submit" class="btn btn-primary">Add Task</button>
                    </form>

                </div>
                
            </div>
            
        </div>
    </div>
    
</div>

@endsection
