@extends('panel.layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">User</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('/users') }}" method="POST">
                        @csrf
                        
                        <div class="form-group">
                            <label for="name">Name<span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" placeholder="Enter name" id="name">
                        </div>
                        
                        <div class="form-group">
                            <label for="name">Email<span class="text-danger">*</span></label>
                            <input type="email" name="email" class="form-control" placeholder="Enter email" id="email">
                        </div>
                        
                        <div class="form-group">
                            <label for="role">Role<span class="text-danger">*</span></label>
                            <select name="role" class="form-control" id="role">
                                <option selected="selected" disabled="disabled">Select a role</option>
                                
                                    @foreach ($roles as $k => $r)
                                        <option value="{{$k}}">{{$r}}</option>
                                    @endforeach    
                                
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="name">Password<span class="text-danger">*</span></label>
                            <input type="password" name="password" class="form-control" placeholder="Enter password" id="password">
                        </div>
                        
                        <div class="form-group">
                            <label for="name">Confirm Password<span class="text-danger">*</span></label>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Enter password" id="conf_password">
                        </div>
                        
                        <button name="submit" value="submit" type="submit" class="btn btn-primary">Create</button>
                    </form>

                </div>
                
            </div>
            
        </div>
    </div>
    
</div>

@endsection