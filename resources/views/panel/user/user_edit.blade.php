@extends('panel.layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">User</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('/users/'.$user_info['id'].'/update') }}" method="POST">
                        @csrf
                        
                        <div class="form-group">
                            <label for="name">Name<span class="text-danger">*</span></label>
                            <input type="text" name="name" value="{{$user_info['name']}}" class="form-control" placeholder="Enter name" id="name">
                        </div>
                        
                        <div class="form-group">
                            <label for="name">Email<span class="text-danger">*</span></label>
                            <input type="email" name="email" value="{{$user_info['email']}}" class="form-control" placeholder="Enter email" id="email">
                        </div>
                        
                        <div class="form-group">
                            <label for="role">Role<span class="text-danger">*</span></label>
                            <select name="role" class="form-control" id="role">
                                <option selected="selected" disabled="disabled">Select a role</option>
                                
                                @foreach ($roles as $k => $r)
                                    <option value="{{$k}}" {{ $role == $k ? "selected" : "" }}>{{$r}}</option>
                                @endforeach
                                
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="name">Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Enter password" id="password">
                        </div>
                        
                        <div class="form-group">
                            <label for="name">Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Enter password" id="conf_password">
                        </div>
                        
                        <button name="submit" value="submit" type="submit" class="btn btn-primary">Update</button>
                        
                    </form>

                </div>
                
                

            </div>
            
        </div>
    </div>
    
</div>

@endsection

@section('javascript')

<script>

    var land_sizes = <?php //echo json_encode($land_sizes); ?>;
    var service_id = <?php echo $user_info['id']; ?>;
    
    //------------------------------------- update details -------------------------------------

    $("#btn-update-details").on('click', function () {

        var csrf_token = $("#csrf_token_update_details").val();  
        
        var detailArray = [];
        var noDuration = true;
        
        if (land_sizes.length > 0) {
            
            $(land_sizes).each(function(index, value) {

                var duration = $('.durations-'+value.id).val();
                var cost = $('.cost-'+value.id).val();
                
                if (duration > 0) {
                    
                    detailArray.push({
                        service_id: service_id,
                        size_id: value.id,
                        hours: duration,
                        price: cost
                    });
                    
                    noDuration = false;
                    
                }
                
            });
            
        }
            
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrf_token
            }
        });
        
        $.ajax({
            url: SITE_URL+'/services/updatedetails',
            type: 'POST',
            dataType: 'json',
            data: {
                service_id : service_id,
                details : detailArray
            }
        }).always(function (data) {
            
            if (data.status) {
                $('#message-success').text(data.message);
                $('#message-success').show('slow').delay(2000).hide('slow');
            } else {
                $('#message-error').text(data.message);
                $('#message-error').show('slow').delay(2000).hide('slow');
            }

        });

        return false;
        
    });

</script>

@endsection