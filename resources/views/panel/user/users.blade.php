@extends('panel.layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">System Users</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">
                
                <span>
                    <div id="message-success" class="alert alert-success" role="alert" style="display: none;"></div>
                    <div id="message-error" class="alert alert-danger" role="alert" style="display: none;"></div>
                </span>

                <div class="card-body">

                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <!--<th scope="col">#</th>-->
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Role</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($users as $u) {
                            ?>
                            <tr id="user-{{$u['id']}}">
                                <!--<td><?php echo $u['id']; ?></td>-->
                                <td><?php echo $u['name']; ?></td>
                                <td><?php echo $u['email']; ?></td>
                                <td><?php echo $u['role']; ?></td>
                                <td>
                                    <?php
                                        $user = Auth::user();
                                    ?>
                                    @if ($user->is_admin == 1)
                                    <a class="btn btn-primary" href="{{ url('/users/'.$u['id']) }}">Edit</a>
                                    @endif
                                    &nbsp;&nbsp;
                                    @if ($u['email'] != "admin@pmanager.com")
                                        <button class="btn btn-danger user-delete" data-id="{{$u['id']}}">Delete</button>
                                        <input type="text" id="csrf_token_delete_user_{{$u['id']}}" name="csrf_token_delete_user_{{$u['id']}}" value="{{csrf_token()}}" hidden/>
                                    @endif
                                </td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>

                </div>

            </div>
            
        </div>
    </div>
    
</div>

@endsection

@section('javascript')

<script>
    
    // delete user
    // -----------------------------------------.
    $(".user-delete").on('click', function () {

        var user_id = $(this).data('id');
        var csrf_token = $("#csrf_token_delete_user_"+user_id).val(); 
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrf_token
            }
        });
        
        $.ajax({
            url: SITE_URL+'/users/'+user_id+'/destroy',
            type: 'POST',
            dataType: 'json',
            data: {
                user_id : user_id
            }
        }).always(function (data) {
            
            if (data.status) {
                $('#user-'+user_id).remove();
                $('#message-success').text(data.message);
                $('#message-success').show('slow').delay(2000).hide('slow');
            } else {
                $('#message-error').text(data.message);
                $('#message-error').show('slow').delay(2000).hide('slow');
            }

        });

        return false;
        
    });
    
</script>

@endsection