<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/home', 'HomeController@panelHome')->name('panel.home')->middleware('is_admin');
Route::get('projectmanager/home', 'HomeController@panelProjectManagerHome')->name('manager.home')->middleware('is_project_manager');

Route::get('/users', 'SystemUserController@index')->name('users.index');
Route::get('/users/create', 'SystemUserController@create')->name('users.create');
Route::post('/users', 'SystemUserController@store')->name('users.store');
Route::get('/users/{id}', 'SystemUserController@edit')->name('users.edit');
Route::post('/users/{id}/update', 'SystemUserController@update')->name('users.update');
Route::post('/users/{id}/destroy', 'SystemUserController@destroy')->name('users.destroy');

Route::get('/projects', 'ProjectController@index')->name('projects.index');
Route::get('/projects/create', 'ProjectController@create')->name('projects.create');
Route::post('/projects', 'ProjectController@store')->name('projects.store');
Route::get('/projects/{id}', 'ProjectController@edit')->name('projects.edit');
Route::get('/projects/{id}/view', 'ProjectController@show')->name('projects.show');
Route::post('/projects/{id}/update', 'ProjectController@update')->name('projects.update');
Route::post('/projects/{id}/destroy', 'ProjectController@destroy')->name('projects.destroy');

Route::get('/tasks', 'TaskController@index')->name('tasks.index');
Route::get('/tasks/create/{id}', 'TaskController@create')->name('tasks.create');
Route::post('/tasks', 'TaskController@store')->name('tasks.store');
Route::get('/tasks/{id}', 'TaskController@edit')->name('tasks.edit');
Route::post('/tasks/{id}/update', 'TaskController@update')->name('tasks.update');
Route::post('/tasks/{id}/destroy', 'TaskController@destroy')->name('tasks.destroy');